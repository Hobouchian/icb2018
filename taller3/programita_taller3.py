#Taller3-Hobouchian María Paula: programa para generar promedios a lo largo de una ventana deslizante
#de tamaño n.
import sys
import datetime
print(sys.argv)

#Parámetros que se reciben por línea de comandos
entrada=open(sys.argv[1])
salida=open(sys.argv[2],'w')
n=int(sys.argv[3])

#Pruebas con valores fijos
#entrada=open('/home/icb14/icb2018/taller3/entrada5.csv')
#salida=open('/home/icb14/icb2018/taller3/salida5.csv','w')
#n=4

#Datos de entrada en Valores y tiempos en TS
TS=[]
Valores=[]
for linea in entrada:
    print(linea.split(','))
    TS.append(linea.split(',')[0])
    Valores.append(linea.split(',')[1:])

#Datos a utilizar en los cálculos
sensores=len(Valores[0])
mediciones=len(TS)
promres=mediciones-n+1
NA='NA'

#Armo tabla resultante res con rangos de tiempo y promedios en la ventana deslizante
res=[]
r=0
while r<promres:
    #Calculos los promedios de los sensores por filas de la ventana
    promfila=[]
    for j in range(sensores):
        acum=0
        for i in range(r,r+n):
            if acum==NA or Valores[i][j].strip('\n')==NA:
                acum=NA
            else:
                acum+=float(Valores[i][j])
        if acum==NA:
            promfila.append(NA)
        else:
            promfila.append("{:.2f}".format(acum/n))    
    #Rango de tiempo en la ventana
    t1=datetime.datetime.strptime(TS[r],'%Y-%m-%dT%H:%M:%S')
    t2=datetime.datetime.strptime(TS[r+n-1],'%Y-%m-%dT%H:%M:%S')
    rango_de_tiempo=(t2-t1).total_seconds() 
    resfila=','.join([str(rango_de_tiempo)]+promfila)       
    res.append(resfila)
    r+=1
    
#Para guardarlo de la forma requerida    
for res_final in res:
    print(res_final,file=salida)
salida.close()