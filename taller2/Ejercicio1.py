#Ejercicio1:
#Función que tome 2 listas de enteros a y b, y que calcule la cantidad
#de elementos de la lista a que se encuentren en b (res).

def cantAparicionesSub(a,b):
    res=0
    #Recorre los elementos de a
    for i in range(len(a)):    
        #Variable ii para considerar una única aparición de cada elem de a en b  
        ii=False
        #Recorre los elementos de b
        for j in range(len(b)):
            if a[i]==b[j] and not ii: #Si elem esta en b y aun no lo consideré
                ii=True
                res=res+1
    return res

a=[1,3,6]
b=[1,1,3,5,7,2,4]
print(cantAparicionesSub(a,b))