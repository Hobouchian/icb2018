#Ejercicio2:
#Funcion que de verdadero si la lista de enteros es creciente hasta cierto 
#elemento y luego decreciente. 
#Importante: se considera lista no vacia que crece estrictamente hasta cierto 
#elemento a partir del cual decrece estrictamente hasta el final de la lista.
#Es decir, si lista es vacia, de 1 o 2 elementos da FALSE. A partir de 3 
#elementos da TRUE si se cumple la condicion. 
def listaTriangular(a):
    i=0
    if len(a)==0 or len(a)==1:
        return False        
    while i<(len(a)-2) and a[i]<a[i+1]: #Puede crecer estrictamente hasta anteultimo elemento
        i=i+1
    while (i>0 and i<(len(a)-1) and a[i]>a[i+1]): #Puede decrecer estrictamente desde segundo elemento 
        i=i+1
    return i==(len(a)-1) #Si recorre toda la lista cumpliendo la condicion de triangular considerada da TRUE

a=[1,4,3]
print(listaTriangular(a))