divisor = 1000
def fix(value):
    return int(value * divisor)

def unfix(value):
    return float(value) / divisor

print((fix(10.05)-fix(15.75))//fix(3.1415))

print(unfix(fix(fix(10.05)-fix(15.75))//fix(3.1415)))

from struct import pack

def verBits(n):
    return ''.join(['{:08b}'.format(c) for c in pack('!f', n)])

float('Nan')

float('inf')

float('-inf')

print(verBits(-1))
