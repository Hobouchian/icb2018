import numpy as np
import time
import random
import sys

global N
#N = 1000
N=10

def matrizCeros():
    a=[]
    for i in range(N):
        b=[]
        for j in range(N):
            b.append(0.0)
        a.append(b)
    return a
              
def matrizAleatoria(a):
    for i in range(len(a)):
        for j in range(len(a[i])):
            a[i][j]=random.random()
    
def multMatrices(a, b, res):
    # COMPLETAR
    pass

def medirTiempos(fn, *args):
    # COMPLETAR
    pass

def realizarExperimento():
    # COMPLETAR
    pass

if __name__ == '__main__':
    if len(sys.argv) > 1:
        N = int(sys.argv[1])
    realizarExperimento()
