#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 10:30:08 2018

@author: icb14
"""
#Ejercicio 2 del TP icb 2018

class ArbolBinario(object):
    """Implementacion de un Arbol binario"""
    
    def __init__(self):
        self.a=None
        self.izq=None
        self.der=None
    
    def vacio(self):
        return (self.a==None and self.izq==None and self.der==None)	
    
    def raiz(self):
        if self.vacio():
            return None
        else:
            return self.a
    
    def bin(self,a,izq,der):
        self.a=a
        self.izq=izq
        self.der=der
    
    def izquierda(self):
        return self.izq

    def derecha(self):
        return self.der

    def find(self,a):
        if self.a()==a:
            return True
        elif self.vacio():
            return False

    def espejo(self):
        return bin(self.a(),self.derecha(),self.izquierda())

    def preorder(self):
        if self.vacio():
            return []
        else:
            return [self.raiz()]+self.izquierda()+self.derecha()
   
    def posorder(self):
    def inorder(self):   
    