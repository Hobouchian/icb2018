#Programa para armar una sumatoria dado un parametro n entero 
def Hazard(n):
    suma=0
    for i in range(1,n+1):
        suma=suma+(((-1)**(i+1))*2/(2*i-1))
    return suma

print(Hazard(3))