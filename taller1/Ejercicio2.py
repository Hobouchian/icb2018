#esPrimo: si suma desde 1 hasta el módulo de x (parametro) de los casos divisibles (resto cero) es 2 devuelve TRUE 
def esPrimo(x):
    suma=0
    for i in range (1,abs(x)+1):
        if abs(x)%i==0: 
            suma=suma+1
    return suma==2        
#print(esPrimo(5))

#esN1: si suma desde n=0 hasta condicion, devuelve TRUE si se cumple la igualdad   
def es2N1(x):
    n=0
    while (2**n-1)<x:
        n=n+1
    return (2**n-1)==x    
#print(es2N1(3))

#Lukaku: dado un valor n>=1, la funcion devuelve el ultimo res al cumplir n veces la condicion es1N1 y esPrimo
#Ej para entender lo que pide: si n=1, res=3; si n=2, res=7 y así.
def Lukaku(n):
    i=0
    suma=0
    while (suma<n): 
        i=i+1
        if (es2N1(i) and esPrimo(i)):
            suma=suma+1
    return i        
#print(Lukaku(2)) 
    
        